Lunch Vote Manager
==================
by rafaelsbz

Sistema para votações no restaurante do almoço.

# Funcionamento

Esse sistema é um serviço REST que possibilita que usuários votem no restaurante que querem almoçar. Ele permite a criação de N votações e que os usuários votem em qualquer uma delas. Além disso, o sistema bloqueia os restaurantes vencedores recentes, pra evitar que um grupo majoritário force todo mundo a almoçar no mesmo lugar todo dia.

Fluxo normal de operação:

- Facilitador **cria** uma Votação (Poll)
	- A qualquer momento, a votação pode ser consultada. É possível ver a data, os votos e o atual vencedor
- Usuários **votam** nos seus restaurantes preferidos naquela poll
- Ao final do prazo, o facilitador **Fecha a Votação**
	- A partir desse momento, aquela votação não aceita mais votos
	- O restaurante vencedor é **bloqueado** em votações futuras
- No outro dia, o facilitador cria uma nova votação e o processo se repete
- Ao final da semana, facilitador **consolida** a semana.
	- Isso desbloqueia todos os restaurantes
	- O facilitador não precisa necessariamente esperar uma semana, ele pode realizar essa operação a qualquer tempo
	- Isso permite uma maior flexibilidade (ex.: podem existir menos de 5 opções de restaurante)
  
O serviço roda na porta 8080. Isso pode ser configurado na propriedade *server.port* do arquivo *application.properties*.
   
A seção a seguir detalha os Endpoints do sistema, que são a interface pelo qual ele pode ser operado.

# Endpoints


### POST `/poll`:

Cria uma votação.

Retorna HTTP 201 com o link para a votação criada no *header* Location e o objeto JSON da votação no corpo da mensagem.

Ex:

```
curl -v -X POST http://localhost:8080/poll/

HTTP/1.1 201 Created
Date: Sun, 08 May 2016 20:51:38 GMT
Location: http://localhost:8080/poll/572fa6da999a17a3433fbf5f
Content-Type: application/json; charset=UTF-8
Content-Length: 263
Server Jetty(9.2.16.v20160414) is not blacklisted
Server: Jetty(9.2.16.v20160414)

STATE: PERFORM => DONE handle 0x6000577c0; line 1645 (connection #0)
Curl_done
Connection #0 to host localhost left intact
Expire cleared

{"id":"572fa6da999a17a3433fbf5f","votes":[],"closed":false,"date":{"month":"MAY","year":2016,"dayOfMonth":8,"dayOfWeek":"SUNDAY","dayOfYear":129,"monthValue":5,"hour":17,"minute":51,"nano":410000000,"second":38,"chronology":{"id":"ISO","calendarType":"iso8601"}}}
```

### GET `/poll/{id}`:

Recupera a votação solicitada com seu atual vencedor. Se a votação estiver fechada (closed = true), isso significa
que aquele é o vencedor do dia.


Exemplos:
```
curl http://localhost:8080/poll/current

ou

curl http://localhost:8080/poll/572fe4d4999accde57845d99


{"id":"572fe4d4999accde57845d99","winner":"pizza","closed":false,"votes":[{"user":"rafael","restaurant":"sushi"},{"user":"daniel","restaurant":"pizza"},{"user":"gabriel","restaurant":"pizza"}]}
```

### POST `/poll/{id}/vote`:

Registra um voto em uma votação. Requer os parâmetros `user` e `restaurant` como FormParameter:

* Se o voto for aceito, retorna HTTP 2OO OK
* Se o voto for recusado, retorna HTTP 403 FORBIDDEN e, no corpo da resposta, um objeto JSON com os motivos da recusa (ex.: se o usuário já votou naquela votação, se o restaurante está bloqueado por ter sido escolhido recentemente)

Exemplos:

#### Voto aceito:

```
$ curl -v --data "user=gabriel&restaurant=pizza" http://localhost:8080/poll/current/vote

{"accepted":true,"reasons":[]}
```

#### Voto rejeitado:

```
curl -v --data "user=rafael&restaurant=sushi" http://localhost:8080/poll/572fe4d4999accde57845d99/vote

{"accepted":false,"reasons":["User rafael already voted in this poll."]}

```

### POST `/poll/{id}/close`:

Fecha uma votação. Isso implica em:

* Os votos são computados e o vencedor é determinado
* O vencedor é bloqueado para novas votações
* A votação fechada passa a não aceitar mais votos

Exemplo:

```
curl -v -X POST http://localhost:8080/poll/572fe4d4999accde57845d99/close

* HTTP 1.1 or later with persistent connection, pipelining supported
< HTTP/1.1 204 No Content
< Date: Mon, 09 May 2016 01:23:54 GMT
* Server Jetty(9.2.16.v20160414) is not blacklisted
< Server: Jetty(9.2.16.v20160414)
<
* STATE: PERFORM => DONE handle 0x6000577c0; line 1645 (connection #0)
* Curl_done
* Connection #0 to host localhost left intact

Recuperando a poll para ver o resultado. Note o valor da propriedade "closed":
curl http://localhost:8080/poll/572fe4d4999accde57845d99
{"id":"572fe4d4999accde57845d99","winner":"pizza","closed":true,"votes":[{"user":"rafael","restaurant":"sushi"},{"user":"daniel","restaurant":"pizza"},{"user":"gabriel","restaurant":"pizza"}]}

```


### GET `/blockedrestaurants`:

Retorna a lista de todos os restaurantes atualmente bloqueados.

Exemplo:

```
curl http://localhost:8080/blockedrestaurants

[{"name":"pizza"}]
```

### GET `/polls`:

Retorna a lista de todos as votações no sistema.

```
$ curl http://localhost:8080/polls

[{"id":"572fe4d4999accde57845d99","votes":[{"user":"rafael","restaurant":"sushi"},{"user":"daniel","restaurant":"pizza"},{"user":"gabriel","restaurant":"pizza"}],"closed":true,"date":{"month":"MAY","year":2016,"dayOfMonth":8,"dayOfWeek":"SUNDAY","dayOfYear":129,"monthValue":5,"hour":22,"minute":16,"nano":837000000,"second":4,"chronology":{"id":"ISO","calendarType":"iso8601"}}}]
```

### POST `/consolidateweek`:

Consolida uma semana, realizando operações necessárias pra fechá-la. Atualmente, apenas limpa a coleção
de restaurantes bloqueados.

Exemplo:

```
curl -X POST http://localhost:8080/consolidateweek

curl http://localhost:8080/blockedrestaurants
[]
```


### Current Poll
Como a maioria dos usuários (provavelmente) é de devs/testers que são familiares com linha de comando, a feature **Current** foi implmentada pra facilitar a criação de scripts.

Em todos os endpoints relacionados a votação (`/poll/{id}`), você pode usar `poll/current` para se referir à votação mais recentemente criada. Por exemplo

- Votar na votação mais recente

```
curl -v --data "user=user&restaurant=sushi" http://localhost:8080/poll/current/vote
```

Isso permite a criação de scripts sem precisar descobrir o ID da votação do dia. 

# Construindo e executando a aplicação

O Lunch Vote Manager foi desenvolvido em Java 8 utilizando Spring Boot e Maven. Portanto, para construí-lo a partir do código fonte, é necessário:

* JDK 8 ou superior
* Maven

A partir daí, basta executar os seguintes comandos do Maven:

#### Executar os testes:

```
mvn clean verify
```



#### Construir a aplicação:

```
mvn clean package
```

Um jar executável deve ser gerado no diretório target/

#### Executar a aplicação usando Maven:

```
mvn spring-boot:run
```

Executa a aplicação na porta 8080



 
# Implementação

Para começar a entender o código do Lunch Vote Manager, a sugestão é começar a ler as implementações dos Endpoints REST e entrar nos componentes. Elas podem ser encontradas nas seguintes classes:

* `PollRestFacade`: Endpoints relativos a uma votação
* `ManagerRestFacade` : Endpoints de gerenciamento

Além disso, o teste de integração principal tem indícios do funcionamento interno do sistema. Ele pode ser encontrado na classe `MainIntegrationTest`

 
Os seguintes frameworks foram usados na implementação:

* Spring Boot: 
	* Criação de serviço REST 
	* Injeção de dependências
	* Versionamento de dependências
	* Logging
	* Abstrações da camada de persistência
* Jersey:
	* Definição de endpoints REST utilizando anotações da especificação JAX-RS
* Embedded MongoDB
	* Executa uma instância do MongoDB em memória
	* Identifica o SO do host da aplicação e configura a versão do MongoDB corretamente
	 
### Embedded MongoDB
O sistema utiliza um MongoDB em memória pra guardar os dados (ex.: votações, votos, restaurantes bloqueados). O código da biblioteca usada pra isso está em: [https://github.com/flapdoodle-oss/de.flapdoodle.embed.mongo](https://github.com/flapdoodle-oss/de.flapdoodle.embed.mongo)

A decisão de usar Embedded MongoDB (ao invés de dados hard-coded, como a especificação sugere) foi baseada nos seguintes pontos:

* A lib, juntamente com o Spring Data, já tem várias implementações prontas para operações comuns que teriam que ser reimplementadas/testadas. Exemplos:
	*  Recuperar um elemento ou toda a coleção
	*  Recuperar uma coleção ordenada
	*  Upsert (inserir ou salvar um elemento se ele não existe)
* Facilita testes de integração fim a fim incluindo persistência
* A aplicação está quase pronta para migrar para um MongoDB externo, necessitando apenas escrever duas configurações Spring simples para acessar o Mongo em produção e usar Embedded Mongo nos testes


### Stateless
Salvo pelo Embedded MongoDB, o código do sistema é *stateless*. Isso é, não há singletons guardando estado em memória. Por exemplo, a votação mais recente é sempre descoberta em tempo de execução a partir do atributo *date*.

Isso permite que, ao migrar o projeto para um BD externo, várias instâncias do serviço sejam usadas para fornecer redundância.      

### Pontos de melhoria

* Criação de um front-end
* Implementação de um scheduler para agendar tarefas do facilitador. Ex.:
	* Criar nova Poll todo dia
	* Fechar poll ao meio dia
	* Fechar a semana aos domingos
	* Sugestões de libs para facilitar essa impl:
		* [Quartz](http://www.quartz-scheduler.org/ "Quartz")
		* [ScheduledExecutorService](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ScheduledExecutorService.html)  
	* É preferível fazer isso em outro módulo para manter o módulo principal stateless 
* Externalizar o MongoDB
* Criar cadastro e validação de restaurantes conhecidos: Isso é interessante porque evita votos errados devido a erros de digitação.
* Melhorar o tratamento dos casos de erro (vários erros retornando 404 com exceções internas ao invés de mensagens claras)
* Melhorar o formato de datas exposto em JSON:
	* Investigar se o formato atual é bem suportado por frameworks JavaScript, senão, usar um formato mais fácil de consumir no front-end


### Problemas conhecidos
* **Case-sensitiveness:** A maioria das validações do sistema não são case-sensitive, então "restaurante" e "Restaurante" registram como lugares diferentes. É necessário normalizar isso