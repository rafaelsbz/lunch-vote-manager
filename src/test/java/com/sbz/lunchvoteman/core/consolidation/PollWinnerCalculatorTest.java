package com.sbz.lunchvoteman.core.consolidation;

import com.google.common.collect.Sets;
import com.sbz.lunchvoteman.ApplicationConfiguration;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.Set;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isOneOf;
import static org.junit.Assert.*;

/**
 * Integration tests for the {@link PollWinnerCalculator}
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfiguration.class)
public class PollWinnerCalculatorTest {

  private static final String USER_1 = "User1";
  private static final String USER_2 = "User2";
  private static final String USER_3 = "User_3";
  private static final String WINNER = "Winner";
  private static final String ANOTHER_WINNER = "AnotherWinner";
  private static final String LOSER = "Loser";
  private static final String ANOTHER_LOSER = "AnotherLoser";

  @Autowired
  private PollWinnerCalculator calculator;

  @Test
  public void shouldCalculateWinner() {
    //Given
    Set<Vote> votes = Sets.newHashSet(new Vote(USER_1, WINNER),
        new Vote(USER_2, WINNER),
        new Vote(USER_2, LOSER),
        new Vote(USER_1, LOSER),
        new Vote(USER_3, ANOTHER_LOSER),
        new Vote(USER_3, WINNER));

    Poll poll = new Poll("ID", votes, LocalDateTime.now());

    //When
    String result = calculator.calculateWinner(poll);

    //Then
    assertThat(result, is(WINNER));
  }

  @Test
  public void shouldReturnAnyOfTheWinnersInCaseOfTie() {
    //Given
    Set<Vote> votes = Sets.newHashSet(
        new Vote(USER_1, WINNER),
        new Vote(USER_2, WINNER),
        new Vote(USER_3, WINNER),
        new Vote(USER_1, ANOTHER_WINNER),
        new Vote(USER_2, ANOTHER_WINNER),
        new Vote(USER_3, ANOTHER_WINNER),
        new Vote(USER_2, LOSER),
        new Vote(USER_1, LOSER),
        new Vote(USER_3, ANOTHER_LOSER));

    Poll poll = new Poll("ID", votes, LocalDateTime.now());

    //When
    String result = calculator.calculateWinner(poll);

    //Then
    assertThat(result, isOneOf(WINNER, ANOTHER_WINNER));
  }

  @Test
  public void shouldReturnEmptyStringIfPollHasNoVotes() {
    //Given;

    Poll emptyPoll = new Poll();

    //When
    String result = calculator.calculateWinner(emptyPoll);

    //Then
    assertThat(result, is(""));
  }

}