package com.sbz.lunchvoteman.core.consolidation;

import com.google.common.collect.Lists;
import com.sbz.lunchvoteman.ApplicationConfiguration;
import com.sbz.lunchvoteman.persistence.entities.BlockedRestaurant;
import com.sbz.lunchvoteman.persistence.repository.BlockedRestaurantRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Integration tests for the {@link ConsolidateWeekOperation}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ApplicationConfiguration.class)
public class ConsolidateWeekOperationTest {

  @Autowired
  private BlockedRestaurantRepository blockedRestaurantRepository;

  @Autowired
  private ConsolidateWeekOperation consolidateWeekOperation;

  @Test
  public void shouldClearBlockedRestaurants() {
    //Given
    blockedRestaurantRepository.save(Lists.newArrayList(
        new BlockedRestaurant("restaurant1"),
        new BlockedRestaurant("restaurant2")
    ));

    //When
    consolidateWeekOperation.consolidateWeek();

    //Then
    assertThat(blockedRestaurantRepository.count(), is(0L));
  }
}