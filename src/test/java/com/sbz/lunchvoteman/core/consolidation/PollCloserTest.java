package com.sbz.lunchvoteman.core.consolidation;

import com.google.common.collect.Sets;
import com.sbz.lunchvoteman.ApplicationConfiguration;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import com.sbz.lunchvoteman.persistence.repository.BlockedRestaurantRepository;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Integration tests for the {@link PollCloser}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfiguration.class)
public class PollCloserTest {

  private static final String POLL_ID = "PollID";

  @Autowired
  private PollRepository pollRepository;

  @Autowired
  private BlockedRestaurantRepository blockedRestaurantRepository;

  @Autowired
  private PollCloser pollCloser;

  @After
  public void clearCollections() {
    pollRepository.deleteAll();
    blockedRestaurantRepository.deleteAll();
  }

  @Test
  public void shouldSaveClosedPoll() {
    //Given
    Poll poll = new Poll(POLL_ID);
    pollRepository.save(poll);

    //When
    pollCloser.closePoll("PollID");

    //Then
    assertThat(pollRepository.findOne(POLL_ID).isClosed(), is(true));
  }

  @Test
  public void shouldBlockRestaurant() {
    //Given
    String winningRestaurantName = "winner";
    Poll poll = new Poll(POLL_ID, Sets.newHashSet(new Vote("user", winningRestaurantName)), LocalDateTime.now());

    pollRepository.save(poll);

    //When
    pollCloser.closePoll("PollID");

    //Then
    assertTrue(blockedRestaurantRepository.exists(winningRestaurantName));
  }

}