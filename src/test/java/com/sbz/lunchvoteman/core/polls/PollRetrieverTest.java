package com.sbz.lunchvoteman.core.polls;

import com.google.common.collect.Sets;
import com.sbz.lunchvoteman.ApplicationConfiguration;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Integration tests for the poll retriever.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ApplicationConfiguration.class)
public class PollRetrieverTest {

  private static final LocalDateTime PRIOR_DATE = LocalDateTime.of(2016, 5, 8, 12, 0);
  private static final LocalDateTime LATEST_DATE = LocalDateTime.of(2017, 10, 10, 12, 0);

  @Autowired
  private PollRepository pollRepository;

  @Autowired
  private PollRetriever pollRetriever;

  @After
  public void clearData() {
    pollRepository.deleteAll();
  }

  @Test
  public void shouldReturnMostRecentPollIfIdEqualsCurrent() {
    //Given
    Poll mostRecentPoll = new Poll("mostRecent", Sets.newHashSet(), LATEST_DATE);

    pollRepository.insert(new Poll("id1", Sets.newHashSet(), PRIOR_DATE));
    pollRepository.insert(new Poll("id2", Sets.newHashSet(), PRIOR_DATE));
    pollRepository.insert(mostRecentPoll);

    //When
    Optional<Poll> result = pollRetriever.retrievePoll("current");

    //Then
    assertThat(result, is(Optional.of(mostRecentPoll)));
  }

  @Test
  public void shouldReturnMostRecentPollIfIdEqualsCurrentWithDifferentCase() {
    //Given
    Poll mostRecentPoll = new Poll("mostRecent", Sets.newHashSet(), LATEST_DATE);

    pollRepository.insert(new Poll("id1", Sets.newHashSet(), PRIOR_DATE));
    pollRepository.insert(new Poll("id2", Sets.newHashSet(), PRIOR_DATE));
    pollRepository.insert(mostRecentPoll);

    //When
    Optional<Poll> result = pollRetriever.retrievePoll("CURReNT");

    //Then
    assertThat(result, is(Optional.of(mostRecentPoll)));
  }

  @Test
  public void shouldReturnByIdIfIdIsNotCurrent() {
    //Given
    Poll mostRecentPoll = new Poll("mostRecent", Sets.newHashSet(), LATEST_DATE);
    Poll expectedPoll = new Poll("id1", Sets.newHashSet(), PRIOR_DATE);

    pollRepository.insert(expectedPoll);
    pollRepository.insert(mostRecentPoll);

    //When
    Optional<Poll> result = pollRetriever.retrievePoll("id1");

    //Then
    assertThat(result, is(Optional.of(expectedPoll)));
  }

  @Test
  public void shouldReturnAbsentIfNoPollExistsForId() {
    //Given
    Poll mostRecentPoll = new Poll("mostRecent", Sets.newHashSet(), LATEST_DATE);
    Poll expectedPoll = new Poll("id1", Sets.newHashSet(), PRIOR_DATE);

    pollRepository.insert(expectedPoll);
    pollRepository.insert(mostRecentPoll);

    //When
    Optional<Poll> result = pollRetriever.retrievePoll("non-existing ID");

    //Then
    assertThat(result, is(Optional.empty()));
  }
}