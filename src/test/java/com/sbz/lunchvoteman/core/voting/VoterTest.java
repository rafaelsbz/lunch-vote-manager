package com.sbz.lunchvoteman.core.voting;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sbz.lunchvoteman.core.polls.PollRetriever;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import com.sbz.lunchvoteman.validation.VoteValidationSuite;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Unit tests for the {@link Voter}.
 */
@RunWith(MockitoJUnitRunner.class)
public class VoterTest {

  private static final String POLL_ID = "1";
  private static final String USER = "USER";
  private static final String RESTAURANT = "RESTAURANT";

  @Mock
  private PollRetriever pollRetriever;
  @Mock
  private PollRepository pollRepository;
  @Mock
  private VoteValidationSuite voteValidationSuite;

  @InjectMocks
  private Voter operationUnderTest;

  private final Poll poll = new Poll(POLL_ID);
  private final Vote vote = new Vote(USER, RESTAURANT);
  private final VoteResult voteAccepted = new VoteResult(true, Lists.newArrayList());
  private final VoteResult voteRejected = new VoteResult(false, Lists.newArrayList("Reasons"));

  @Test
  public void shouldReturnAcceptedIfVoteIsValid() {
    //Given
    Optional<Poll> optionalOfPoll = Optional.of(poll);

    given(pollRetriever.retrievePoll(POLL_ID)).willReturn(optionalOfPoll);
    given(voteValidationSuite.validate(vote, optionalOfPoll)).willReturn(voteAccepted);

    //When
    VoteResult result = operationUnderTest.registerVote(POLL_ID, vote);

    //Then
    assertThat(result, is(voteAccepted));
  }

  @Test
  public void shouldUpdatePollIfVoteIsAccepted() {
    //Given
    Optional<Poll> optionalOfPoll = Optional.of(poll);

    given(pollRetriever.retrievePoll(POLL_ID)).willReturn(optionalOfPoll);
    given(voteValidationSuite.validate(vote, optionalOfPoll)).willReturn(voteAccepted);

    Poll updatedPoll = new Poll(POLL_ID, Sets.newHashSet(vote), poll.getDate());

    //When
    operationUnderTest.registerVote(POLL_ID, vote);

    //Then
    verify(pollRepository).save(updatedPoll);
  }

  @Test
  public void shouldNotUpdatePollIfVoteIsRejected() {
    //Given
    Optional<Poll> optionalOfPoll = Optional.of(poll);

    given(pollRetriever.retrievePoll(POLL_ID)).willReturn(optionalOfPoll);
    given(voteValidationSuite.validate(vote, optionalOfPoll)).willReturn(voteRejected);

    //When
    operationUnderTest.registerVote(POLL_ID, vote);

    //Then
    verify(pollRepository, never()).save(any(Poll.class));
  }
}