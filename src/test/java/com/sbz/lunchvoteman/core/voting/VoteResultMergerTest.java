package com.sbz.lunchvoteman.core.voting;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * Unit tests for the {@link VoteResultMerger} operator.
 */
public class VoteResultMergerTest {

  private static final String REASON_1 = "Reason1";
  private static final String REASON_2 = "Reason2";
  private static final String REASON_3 = "Reason3";

  private final VoteResultMerger merger = new VoteResultMerger();

  @Test
  public void shouldReturnAcceptedTrueIfBothInputsAreAccepted() {
    //Given
    VoteResult result = new VoteResult(true, new ArrayList<>());
    VoteResult otherResult = new VoteResult(true, new ArrayList<>());

    //When
    VoteResult apply = merger.apply(result, otherResult);

    //Then
    assertThat(apply.isAccepted(), is(true));
  }

  @Test
  public void shouldReturnAcceptedFalseIfOneInputWasRejected() {
    //Given
    VoteResult result = new VoteResult(true, new ArrayList<>());
    VoteResult otherResult = new VoteResult(false, new ArrayList<>());

    //When
    VoteResult apply = merger.apply(result, otherResult);

    //Then
    assertThat(apply.isAccepted(), is(false));
  }

  @Test
  public void shouldReasonsFromBothResults() {
    //Given
    VoteResult result = new VoteResult(true, Lists.newArrayList(REASON_1));
    VoteResult otherResult = new VoteResult(false, Lists.newArrayList(REASON_2, REASON_3));

    //When
    VoteResult apply = merger.apply(result, otherResult);

    //Then
    assertThat(apply.getReasons(), hasItems(REASON_1, REASON_2, REASON_3));
  }
}