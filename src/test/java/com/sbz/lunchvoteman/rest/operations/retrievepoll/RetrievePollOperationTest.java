package com.sbz.lunchvoteman.rest.operations.retrievepoll;

import com.sbz.lunchvoteman.core.consolidation.PollWinnerCalculator;
import com.sbz.lunchvoteman.core.polls.PollRetriever;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import com.sbz.lunchvoteman.rest.operations.retrievepoll.RetrievePollOperation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

/**
 * Unit tests for the {@link retrievepoll}.
 */
@RunWith(MockitoJUnitRunner.class)
public class RetrievePollOperationTest {

  private static final String ID = "id";
  private static final String WINNER = "Winner";

  @Mock
  private PollRetriever pollRetriever;

  @Mock
  private PollRepository repository;

  @Mock
  private PollWinnerCalculator winnerCalculator;

  @InjectMocks
  private RetrievePollOperation operation;

  @Test
  public void shouldReturnPoll() {
    //Given
    Poll aPoll = new Poll();
    PollWithWinner expectedEntity = new PollWithWinner(aPoll, WINNER);

    given(pollRetriever.retrievePoll(ID)).willReturn(Optional.of(aPoll));
    given(winnerCalculator.calculateWinner(aPoll)).willReturn(WINNER);

    //When
    Response response = operation.retrievePoll(ID);

    //Then
    assertThat(response.getStatus(), is(200));
    assertThat(response.getEntity(), is(expectedEntity));
  }

  @Test
  public void shouldReturnHttp404IfPollWasNotFound() {
    //Given
    given(pollRetriever.retrievePoll(ID)).willReturn(Optional.empty());

    //When
    Response response = operation.retrievePoll(ID);

    //Then
    assertThat(response.getStatus(), is(404));
  }
}