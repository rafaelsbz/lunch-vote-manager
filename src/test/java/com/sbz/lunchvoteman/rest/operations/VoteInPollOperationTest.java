package com.sbz.lunchvoteman.rest.operations;

import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.core.voting.Voter;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

/**
 * Unit tests for the {@link VoteInPollOperation}.
 */
@RunWith(MockitoJUnitRunner.class)
public class VoteInPollOperationTest {

  private static final String POLL_ID = "pollID";
  private static final String USER = "user";
  private static final String RESTAURANT = "restaurant";

  @Mock
  private Voter voter;

  @InjectMocks
  private VoteInPollOperation operation;

  @Test
  public void shouldReturn200OKIfVoteIsAccepted() {
    //Given
    Vote vote =  new Vote(USER, RESTAURANT);
    given(voter.registerVote(POLL_ID, vote)).willReturn(VoteResult.accepted());

    //When
    Response response = operation.registerVote(POLL_ID, USER, RESTAURANT);

    //Then
    assertThat(response.getStatus(), is(200));
    assertThat(response.getEntity(), is(VoteResult.accepted()));
  }

  @Test
  public void shouldReturn403ForbiddenIfVoteIsRejected() {
    //Given
    Vote vote =  new Vote(USER, RESTAURANT);
    given(voter.registerVote(POLL_ID, vote)).willReturn(VoteResult.rejected("Reasons"));

    //When
    Response response = operation.registerVote(POLL_ID, USER, RESTAURANT);

    //Then
    assertThat(response.getStatus(), is(403));
    assertThat(response.getEntity(), is(VoteResult.rejected("Reasons")));
  }
}