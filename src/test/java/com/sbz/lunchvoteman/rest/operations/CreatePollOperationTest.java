package com.sbz.lunchvoteman.rest.operations;

import com.sbz.lunchvoteman.ApplicationConfiguration;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.ws.rs.core.Response;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

/**
 * Integration tests for the {@link CreatePollOperation}.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ApplicationConfiguration.class)
public class CreatePollOperationTest {

  @Autowired
  private PollRepository pollRepository;

  @Autowired
  private CreatePollOperation operationUnderTest;

  @After
  public void clearCollection() {
    pollRepository.deleteAll();
  }

  @Test
  public void shouldCreateAPoll() {
    //When
    operationUnderTest.createNewPoll();

    //Then
    assertThat(pollRepository.findAll(), is(not(empty())));
  }

  @Test
  public void shouldReturn201CreatedWithPollRelativeUri() {
    //When
    Response response = operationUnderTest.createNewPoll();
    Poll createdPoll = pollRepository.findAll().stream().findFirst().get();

    //Then
    assertThat(response.getStatus(), is(201));
    assertThat(response.getLocation().toString(), containsString(createdPoll.getId()));
  }

  @Test
  public void shouldReturnPollInResponseBody() {
    //When
    Response response = operationUnderTest.createNewPoll();
    Poll createdPoll = pollRepository.findAll().stream().findFirst().get();

    //Then
    assertThat(response.getEntity(), is(createdPoll));
  }
}