package com.sbz.lunchvoteman.validation;

import com.google.common.collect.Lists;
import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.core.voting.VoteResultMerger;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;

/**
 * Unit tests for the {@link VoteValidationSuite}.
 */
@RunWith(MockitoJUnitRunner.class)
public class VoteValidationSuiteTest {

  @Mock
  private VoteValidator validator;
  @Mock
  private VoteValidator anotherValidator;
  @Mock
  private VoteResultMerger resultMerger;


  private VoteValidationSuite validationSuite;

  private VoteResult mergedVoteResult = new VoteResult(true, Lists.newArrayList("mergedReasons"));
  private VoteResult aResult;
  private VoteResult anotherResult;

  @Before
  public void setup() {
    given(resultMerger.apply(anyObject(), anyObject())).willReturn(VoteResult.rejected());
    List<VoteValidator> validators = Lists.newArrayList(validator, anotherValidator);
    validationSuite = new VoteValidationSuite(validators, resultMerger);
  }

  @Test
  public void shouldApplyAllValidators() {
    //Given
    Vote vote = new Vote("user", "restaurant");
    Poll poll = new Poll();

    //When
    validationSuite.validate(vote, Optional.of(poll));

    //Then
    verify(validator).validate(vote, poll);
    verify(anotherValidator).validate(vote, poll);
  }

  @Test
  public void shouldReturnMergedResultOfValidators() {
    //Given
    Vote vote = new Vote("user", "restaurant");
    Poll poll = new Poll();

    given(validator.validate(vote, poll)).willReturn(aResult);
    given(validator.validate(vote, poll)).willReturn(anotherResult);
    given(resultMerger.apply(aResult, anotherResult)).willReturn(mergedVoteResult);

    //When
    VoteResult result = validationSuite.validate(vote, Optional.of(poll));

    //Then
    assertThat(result, is(mergedVoteResult));
  }

  @Test
  public void shouldReturnAcceptedWhenNoValidatorsAreCalled() {
    //Given
    Vote vote = new Vote("user", "restaurant");
    Poll poll = new Poll();

    ArrayList<VoteValidator> emptyListOfValidators = new ArrayList<>();
    validationSuite = new VoteValidationSuite(emptyListOfValidators, resultMerger);

    //When
    VoteResult result = validationSuite.validate(vote, Optional.of(poll));

    //Then
    assertThat(result, is(VoteResult.accepted()));
  }
}