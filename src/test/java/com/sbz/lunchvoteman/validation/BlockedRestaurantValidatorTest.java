package com.sbz.lunchvoteman.validation;

import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import com.sbz.lunchvoteman.persistence.repository.BlockedRestaurantRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.*;

/**
 * Unit tests for the {@link BlockedRestaurantValidator}.
 */
@RunWith(MockitoJUnitRunner.class)
public class BlockedRestaurantValidatorTest {

  private static final String RESTAURANT_NAME = "RestaurantName";

  @Mock
  private BlockedRestaurantRepository blockedRestaurantRepository;

  @Mock
  private Poll poll;

  @InjectMocks
  private BlockedRestaurantValidator validator;

  @Test
  public void shouldAcceptVoteIfRestaurantIsNotInBlockedRestaurantsCollection() {
    //Given
    given(blockedRestaurantRepository.exists(RESTAURANT_NAME)).willReturn(false);

    //When
    VoteResult result = validator.validate(new Vote("user", RESTAURANT_NAME), poll);

    //Then
    assertThat(result, is(VoteResult.accepted()));
  }

  @Test
  public void shouldRejectVoteIfRestaurantIsInBlockedRestaurantsCollection() {
    //Given
    given(blockedRestaurantRepository.exists(RESTAURANT_NAME)).willReturn(true);

    //When
    VoteResult result = validator.validate(new Vote("user", RESTAURANT_NAME), poll);

    //Then
    assertThat(result.isAccepted(), is(false));
    assertThat(result.getReasons(), is(not(empty())));
  }
}