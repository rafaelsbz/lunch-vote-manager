package com.sbz.lunchvoteman.validation;

import com.google.common.collect.Sets;
import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

/**
 * Unit tests for the {@link OneVotePerUserValidator}
 */
public class OneVotePerUserValidatorTest {

  private static final String USER_1 = "User1";
  private static final String USER_2 = "User2";
  private static final String USER_3 = "User3";
  private static final String RESTAURANT_1 = "Restaurant1";
  private static final String RESTAURANT_2 = "Restaurant2";
  private static final String RESTAURANT_3 = "Restaurant3";

  private final OneVotePerUserValidator validator = new OneVotePerUserValidator();


  @Test
  public void shouldReturnAcceptedIfUserDidntVoteInPoll() {
    //Given
    Poll poll = new Poll("ID", Sets.newHashSet(
        new Vote(USER_1, RESTAURANT_1),
        new Vote(USER_2, RESTAURANT_2),
        new Vote(USER_3, RESTAURANT_3)
    ), LocalDateTime.now());

    //When
    VoteResult result = validator.validate(new Vote("A new user", RESTAURANT_1), poll);

    //Then
    assertThat(result, is(VoteResult.accepted()));
  }

  @Test
  public void shouldReturnRejectedIfUserAlreadyVotedInPoll() {
    //Given
    Poll poll = new Poll("ID", Sets.newHashSet(
        new Vote(USER_1, RESTAURANT_1),
        new Vote(USER_2, RESTAURANT_2),
        new Vote(USER_3, RESTAURANT_3)
    ), LocalDateTime.now());

    //When
    VoteResult result = validator.validate(new Vote(USER_1, RESTAURANT_2), poll);

    //Then
    assertThat(result.isAccepted(), is(false));
    assertThat(result.getReasons(), is(not(empty())));
  }
}