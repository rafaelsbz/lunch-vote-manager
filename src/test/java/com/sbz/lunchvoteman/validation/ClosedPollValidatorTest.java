package com.sbz.lunchvoteman.validation;

import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import org.junit.Test;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

/**
 * Unit tests for the {@link ClosedPollValidator}.
 */
public class ClosedPollValidatorTest {

  private final ClosedPollValidator validator = new ClosedPollValidator();

  @Test
  public void shouldAcceptVoteIfPollIsNotClosed() {
    //Given
    Poll poll = new Poll();
    poll.setClosed(false);

    //When
    VoteResult result = validator.validate(new Vote("doesnt", "matter"), poll);

    //Then
    assertThat(result, is(VoteResult.accepted()));
  }

  @Test
  public void shouldRejectVoteIfPollIsClosed() {
    //Given
    Poll poll = new Poll();
    poll.setClosed(true);

    //When
    VoteResult result = validator.validate(new Vote("doesnt", "matter"), poll);

    //Then
    assertThat(result.isAccepted(), is(false));
    assertThat(result.getReasons(), is(not(empty())));
  }
}