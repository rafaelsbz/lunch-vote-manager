package com.sbz.lunchvoteman;

import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.repository.BlockedRestaurantRepository;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import com.sbz.lunchvoteman.rest.facade.ManagerRestFacade;
import com.sbz.lunchvoteman.rest.facade.PollRestFacade;
import com.sbz.lunchvoteman.rest.operations.retrievepoll.PollWithWinner;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.ws.rs.core.Response;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Integration tests that exercises the system common usage.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ApplicationConfiguration.class)
public class MainIntegrationTest {

  private static final String CURRENT = "current";

  private static final String USER_1 = "user1";
  private static final String USER_2 = "user2";
  private static final String USER_3 = "user3";
  private static final String USER_4 = "user4";
  private static final String USER_5 = "user5";

  private static final String LOSER = "loser";
  private static final String OTHER_LOSER = "other loser";
  private static final String WINNER = "winner";
  private static final String OLD_WINNER = "old winner";

  @Autowired
  private PollRestFacade pollRestFacade;

  @Autowired
  private ManagerRestFacade managerRestFacade;

  @Autowired
  private PollRepository pollRepository;

  @Autowired
  private BlockedRestaurantRepository blockedRestaurantRepository;

  @After
  public void after() {
    pollRepository.deleteAll();
    blockedRestaurantRepository.deleteAll();
  }

  // Validates Acceptance Criteria Three
  @Test
  public void usersShouldBeAbleToVoteOnCurrentPollAndFindOutTheWinner() {
    //Given
    pollRestFacade.createPoll();

    //When
    pollRestFacade.vote(CURRENT, USER_1, LOSER);
    pollRestFacade.vote(CURRENT, USER_2, LOSER);
    pollRestFacade.vote(CURRENT, USER_3, WINNER);
    pollRestFacade.vote(CURRENT, USER_4, WINNER);
    pollRestFacade.vote(CURRENT, USER_5, OTHER_LOSER);
    pollRestFacade.closePoll(CURRENT);

    //Then
    PollWithWinner poll = (PollWithWinner) pollRestFacade.getPoll(CURRENT).getEntity();

    assertThat(poll.getWinner(), is(WINNER));
    assertThat(poll.getVotes(), hasSize(5));
    assertThat(poll.isClosed(), is(true));
  }

  @Test
  public void usersShouldBeAbleToVoteOnOlderPoll() {
    //Given

    //Old poll
    Poll oldPoll = (Poll) pollRestFacade.createPoll().getEntity();
    String oldPollId = oldPoll.getId();

    //Create new Poll
    pollRestFacade.createPoll();

    //When
    pollRestFacade.vote(oldPollId, USER_1, LOSER);
    pollRestFacade.vote(oldPollId, USER_2, LOSER);
    pollRestFacade.vote(oldPollId, USER_3, WINNER);
    pollRestFacade.vote(oldPollId, USER_4, WINNER);
    pollRestFacade.vote(oldPollId, USER_5, OTHER_LOSER);
    pollRestFacade.closePoll(oldPollId);

    //Then
    PollWithWinner oldPollWithWinner = (PollWithWinner) pollRestFacade.getPoll(oldPollId).getEntity();
    PollWithWinner currentPoll = (PollWithWinner) pollRestFacade.getPoll(CURRENT).getEntity();


    assertThat(oldPollWithWinner.getWinner(), is(WINNER));
    assertThat(oldPollWithWinner.getVotes(), hasSize(5));
    assertThat(oldPollWithWinner.isClosed(), is(true));

    assertThat(currentPoll.getWinner(), is(""));
    assertThat(currentPoll.getVotes(), hasSize(0));
    assertThat(currentPoll.isClosed(), is(false));
  }

  // Validates Acceptance Criteria One
  @Test
  public void shouldNotAllowUserToVoteTwiceOnSamePoll() {
    //Given
    pollRestFacade.createPoll();
    pollRestFacade.vote(CURRENT, USER_1, LOSER);
    pollRestFacade.vote(CURRENT, USER_1, LOSER);
    pollRestFacade.vote(CURRENT, USER_1, LOSER);
    pollRestFacade.vote(CURRENT, USER_2, WINNER);
    pollRestFacade.vote(CURRENT, USER_3, WINNER);

    //When
    PollWithWinner poll = (PollWithWinner) pollRestFacade.getPoll(CURRENT).getEntity();

    //Then
    assertThat(poll.getWinner(), is(WINNER));
    assertThat(poll.getVotes(), hasSize(3));
  }

  //Validates Acceptance Criteria Two
  @Test
  public void shouldNotAllowUserToVoteInPreviousWinner() {
    //Given
    //First day poll
    Poll oldPoll = (Poll) pollRestFacade.createPoll().getEntity();
    String oldPollId = oldPoll.getId();

    //First poll
    pollRestFacade.vote(CURRENT, USER_1, LOSER);
    pollRestFacade.vote(CURRENT, USER_2, WINNER);
    pollRestFacade.vote(CURRENT, USER_3, OLD_WINNER);
    pollRestFacade.vote(CURRENT, USER_4, OLD_WINNER);
    pollRestFacade.closePoll(CURRENT);

    // Another day, new Poll.
    pollRestFacade.createPoll();
    Response rejectedVoteResponse = pollRestFacade.vote(CURRENT, USER_1, OLD_WINNER);
    pollRestFacade.vote(CURRENT, USER_2, WINNER);
    pollRestFacade.vote(CURRENT, USER_3, LOSER);
    pollRestFacade.vote(CURRENT, USER_4, WINNER);
    pollRestFacade.closePoll(CURRENT);

    //When
    PollWithWinner oldPollWithWinner = (PollWithWinner) pollRestFacade.getPoll(oldPollId).getEntity();
    PollWithWinner currentPoll = (PollWithWinner) pollRestFacade.getPoll(CURRENT).getEntity();
    VoteResult voteOnPreviousWinnerResult = (VoteResult) rejectedVoteResponse.getEntity();


    //Then
    assertThat(oldPollWithWinner.getWinner(), is(OLD_WINNER));
    assertThat(oldPollWithWinner.getVotes(), hasSize(4));
    assertThat(oldPollWithWinner.isClosed(), is(true));

    assertThat(currentPoll.getWinner(), is(WINNER));
    assertThat(currentPoll.getVotes(), hasSize(3));
    assertThat(currentPoll.isClosed(), is(true));

    assertThat(voteOnPreviousWinnerResult.isAccepted(), is(false));
  }

}
