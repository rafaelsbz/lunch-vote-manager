package com.sbz.lunchvoteman.rest.facade;

import com.sbz.lunchvoteman.core.consolidation.PollCloser;
import com.sbz.lunchvoteman.rest.operations.CreatePollOperation;
import com.sbz.lunchvoteman.rest.operations.VoteInPollOperation;
import com.sbz.lunchvoteman.rest.operations.retrievepoll.RetrievePollOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Contains the REST Endpoints that deal with poll management.
 */
@Component
@Path("/poll")
public class PollRestFacade {

  private static final Logger LOGGER = LoggerFactory.getLogger(PollRestFacade.class);

  private CreatePollOperation createPollOperation;
  private RetrievePollOperation retrievePollOperation;
  private PollCloser pollCloser;
  private VoteInPollOperation voteInPollOperation;

  @Autowired
  public PollRestFacade(CreatePollOperation createPollOperation, RetrievePollOperation retrievePollOperation,
      PollCloser pollCloser, VoteInPollOperation voteInPollOperation) {
    this.createPollOperation = createPollOperation;
    this.retrievePollOperation = retrievePollOperation;
    this.pollCloser = pollCloser;
    this.voteInPollOperation = voteInPollOperation;
  }

  /**
   * Creates a poll.
   */
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public Response createPoll() {
    LOGGER.info("Create Poll operation requested");
    return createPollOperation.createNewPoll();
  }

  /**
   * Retrives a poll, or the most recent poll if the param id is 'current'.
   */
  @GET
  @Path("/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getPoll(@PathParam("id") String id) {
    LOGGER.info("Requested Poll with id '{}'", id);
    return retrievePollOperation.retrievePoll(id);
  }

  /**
   * Votes on a poll. Returns a {@link com.sbz.lunchvoteman.core.voting.VoteResult} in JSON detailing
   * if the vote was accepted or rejected.
   */
  @POST
  @Path("/{id}/vote")
  @Produces(MediaType.APPLICATION_JSON)
  public Response vote(
      @PathParam("id") String id,
      @FormParam("user") String user,
      @FormParam("restaurant") String restaurant) {
    LOGGER.info("User '{}' submitted a vote for '{}' in poll '{}'", user, restaurant, id);
    return voteInPollOperation.registerVote(id, user, restaurant);
  }

  /**
   * Closes a poll.
   */
  @POST
  @Path("/{id}/close")
  public void closePoll(@PathParam("id") String id) {
    //[FIXME] Implementar erros mais precisos quando a poll não existe ou já foi fechada.
    LOGGER.info("Close Poll {} requested", id);
    pollCloser.closePoll(id);
    LOGGER.info("Poll {} closed", id);
  }
}
