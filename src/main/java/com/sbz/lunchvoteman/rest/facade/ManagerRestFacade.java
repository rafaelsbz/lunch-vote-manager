package com.sbz.lunchvoteman.rest.facade;

import com.google.common.collect.Lists;
import com.sbz.lunchvoteman.core.consolidation.ConsolidateWeekOperation;
import com.sbz.lunchvoteman.persistence.entities.BlockedRestaurant;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.repository.BlockedRestaurantRepository;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Defines the REST operations that should be performed by a manager, such as closing a week.
 */
@Component
@Path("/")
public class ManagerRestFacade {

  private static Logger LOGGER = LoggerFactory.getLogger(ManagerRestFacade.class);

  private ConsolidateWeekOperation consolidateWeekOperation;
  private BlockedRestaurantRepository blockedRestaurantRepository;
  private PollRepository pollRepository;

  @Autowired
  public ManagerRestFacade(ConsolidateWeekOperation consolidateWeekOperation,
      BlockedRestaurantRepository blockedRestaurantRepository, PollRepository pollRepository) {
    this.consolidateWeekOperation = consolidateWeekOperation;
    this.blockedRestaurantRepository = blockedRestaurantRepository;
    this.pollRepository = pollRepository;
  }

  /**
   * Consolidates a week in the system, performing management operations such as cleaning the list
   * of blocked restaurants.
   */
  @POST
  @Path("/consolidateweek")
  public void consolidateWeek() {
    LOGGER.info("Received a request to consolidate week.");
    consolidateWeekOperation.consolidateWeek();
  }

  /**
   * Produces a list of all currently blocked restaurants.
   */
  @GET
  @Path("/blockedrestaurants")
  @Produces(MediaType.APPLICATION_JSON)
  public List<BlockedRestaurant> blockedRestaurants() {
    LOGGER.info("A list of currently blocked restaurants was requested");
    return Lists.newArrayList(blockedRestaurantRepository.findAll());
  }

  /**
   * Produces a list of all polls
   */
  @GET
  @Path("/polls")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Poll> polls() {
    LOGGER.info("A list of all polls was requested");
    return Lists.newArrayList(pollRepository.findAll());
  }
}
