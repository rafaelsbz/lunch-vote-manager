package com.sbz.lunchvoteman.rest.operations;

import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Creates a Poll.
 */
@Component
public class CreatePollOperation {

  private static final Logger LOGGER = LoggerFactory.getLogger(CreatePollOperation.class);

  private final PollRepository pollRepository;

  @Autowired
  public CreatePollOperation(PollRepository pollRepository) {
    this.pollRepository = pollRepository;
  }

  public Response createNewPoll() {
    Poll created = pollRepository.insert(new Poll());

    LOGGER.info("New Poll created with id: {}", created.getId());

    return createResponse(created);
  }

  private Response createResponse(Poll poll) {
    String relativeUri = formatUri(poll);

    try {
      return Response.created(new URI(relativeUri)).entity(poll).build();
    } catch (URISyntaxException e) {
      LOGGER.error("Could not create URI for Poll '{}'. It might not be acessible. Create another one.", relativeUri);
      return Response.serverError().build();
    }
  }

  private String formatUri(Poll created) {
    return String.format("poll/%s", created.getId());
  }

}
