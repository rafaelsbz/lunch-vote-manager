package com.sbz.lunchvoteman.rest.operations.retrievepoll;

import com.google.common.base.Objects;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;

/**
 * Represents a Poll with its current Winner. Should be used only to output results.
 */
public class PollWithWinner {

  private final String id;
  private final String winner;
  private final boolean closed;
  private final Set<Vote> votes;
  private final LocalDateTime dateAndTime;

  PollWithWinner(String id, String winner, boolean closed,
      Set<Vote> votes, LocalDateTime dateAndTime) {
    this.id = id;
    this.winner = winner;
    this.closed = closed;
    this.votes = votes;
    this.dateAndTime = dateAndTime;
  }

  PollWithWinner(Poll poll, String winner) {
    this(poll.getId(), winner, poll.isClosed(), poll.getVotes(), poll.getDate());
  }

  public String getId() {
    return id;
  }

  public String getWinner() {
    return winner;
  }

  public boolean isClosed() {
    return closed;
  }

  public Set<Vote> getVotes() {
    return votes;
  }

  public LocalDateTime getDateAndTime() {
    return dateAndTime;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof PollWithWinner)) {
      return false;
    }
    PollWithWinner that = (PollWithWinner) o;
    return closed == that.closed &&
        Objects.equal(id, that.id) &&
        Objects.equal(winner, that.winner) &&
        Objects.equal(votes, that.votes) &&
        Objects.equal(dateAndTime, that.dateAndTime);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, winner, closed, votes, dateAndTime);
  }
}
