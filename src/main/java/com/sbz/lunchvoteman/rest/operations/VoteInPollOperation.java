package com.sbz.lunchvoteman.rest.operations;

import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.core.voting.Voter;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;

/**
 * Registers a vote in a poll.
 */
@Component
public class VoteInPollOperation {

  private final Voter voter;

  @Autowired
  public VoteInPollOperation(Voter voter) {
    this.voter = voter;
  }

  /**
   * Registers a vote. Returns a 200 OK response if the vote was accepted or a 403 FORBIDDEN if the vote was rejected.
   * Regardless of the result, a {@link com.sbz.lunchvoteman.core.voting.VoteResult} object is returned in the body of the
   * Response.
   */
  public Response registerVote(String pollId, String user, String restaurant) {
    Vote vote = new Vote(user, restaurant);
    VoteResult voteResult = voter.registerVote(pollId, vote);

    return createResponseStatusFromResult(voteResult.isAccepted()).entity(voteResult).build();
  }

  private Response.ResponseBuilder createResponseStatusFromResult(boolean voteAccepted) {
    if (voteAccepted) {
      return Response.ok();
    }
    return Response.status(Response.Status.FORBIDDEN);
  }
}
