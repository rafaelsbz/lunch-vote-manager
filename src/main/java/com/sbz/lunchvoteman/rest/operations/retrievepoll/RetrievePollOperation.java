package com.sbz.lunchvoteman.rest.operations.retrievepoll;

import com.sbz.lunchvoteman.core.consolidation.PollWinnerCalculator;
import com.sbz.lunchvoteman.core.polls.PollRetriever;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.Optional;

/**
 * Retrieves a poll.
 */
@Component
public class RetrievePollOperation {

  private static Logger LOGGER = LoggerFactory.getLogger(RetrievePollOperation.class);

  private final PollRetriever retriever;
  private final PollWinnerCalculator winnerCalculator;

  @Autowired
  public RetrievePollOperation(PollRetriever retriever, PollWinnerCalculator winnerCalculator) {
    this.retriever = retriever;
    this.winnerCalculator = winnerCalculator;
  }

  /**
   * Retrieves a poll from the repository. Returns an HTTP OK response with the poll and its current winner or
   * an HTTP 404 if the poll was not found.
   */
  public Response retrievePoll(String id) {
    Optional<Poll> retrievedPoll = retriever.retrievePoll(id);
    Response response;

    if (retrievedPoll.isPresent()) {
      Poll poll = retrievedPoll.get();
      LOGGER.info("Found Poll {}. Will calculate current winner.", poll);
      String winner = winnerCalculator.calculateWinner(poll);
      PollWithWinner pollWithWinner = new PollWithWinner(poll, winner);

      LOGGER.info("Current winner for Poll {} is {}. Returning this information", poll.getId(), winner);

      response = Response.ok(pollWithWinner).build();
    } else {
      LOGGER.info("Poll {} not found. Returning HTTP 404", id);
      response = Response.status(Response.Status.NOT_FOUND).build();
    }

    return response;
  }

}
