package com.sbz.lunchvoteman.springconfigurations;

import com.sbz.lunchvoteman.rest.facade.ManagerRestFacade;
import com.sbz.lunchvoteman.rest.facade.PollRestFacade;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 * Spring Configuration to use Jersey instead of SpringMVC with Spring Boot.
 */
@Component
public class JerseyConfiguration extends ResourceConfig {

  /**
   * Builds the configuration and registers the Endpoints.
   */
  public JerseyConfiguration() {
    register(PollRestFacade.class);
    register(ManagerRestFacade.class);
  }
}
