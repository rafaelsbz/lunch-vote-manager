package com.sbz.lunchvoteman.validation;

import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.core.voting.VoteResultMerger;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Responsible for validating a registerVote in a poll.
 */
@Component
public class VoteValidationSuite {

  /*
   * This is not obvious, so it's worth explaining:
   * When you inject a List<Interface> with Spring, it actually injects all Components that
   * instantiate the given Interface and are available in the Spring Context.
   *
   * This allows new validations to be registered without static methods. One only needs to
   * implement the interface.
   */
  private final List<VoteValidator> voteValidators;

  private final VoteResultMerger resultMerger;

  @Autowired
  public VoteValidationSuite(List<VoteValidator> voteValidators, VoteResultMerger resultMerger) {
    this.voteValidators = voteValidators;
    this.resultMerger = resultMerger;
  }

  public VoteResult validate(Vote vote, Optional<Poll> poll) {
    //TODO Implement edge-case of Poll not found.
    return voteValidators.stream()
        .map(validator -> validator.validate(vote, poll.get()))
        .reduce(resultMerger)
        .orElse(VoteResult.accepted());
  }

}
