package com.sbz.lunchvoteman.validation;

import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import com.sbz.lunchvoteman.persistence.repository.BlockedRestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Validator that prevents votes in isClosed restaurants. They are usually isClosed when they've
 * recently been winners of a poll.
 */
@Component
public class BlockedRestaurantValidator implements VoteValidator {

  private final BlockedRestaurantRepository blockedRestaurantRepository;

  @Autowired
  public BlockedRestaurantValidator(BlockedRestaurantRepository blockedRestaurantRepository) {
    this.blockedRestaurantRepository = blockedRestaurantRepository;
  }

  @Override
  public VoteResult validate(Vote vote, Poll poll) {
    if (blockedRestaurantRepository.exists(vote.getRestaurant())) {
      return VoteResult.rejected(String.format(
          "The restaurant '%s' is blocked because it recently won a poll. Wait until next week.",
          vote.getRestaurant()));
    }
    return VoteResult.accepted();
  }
}
