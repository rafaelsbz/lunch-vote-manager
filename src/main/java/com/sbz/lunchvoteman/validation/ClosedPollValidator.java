package com.sbz.lunchvoteman.validation;

import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import org.springframework.stereotype.Component;

/**
 * Responsible for rejecting votes on isClosed polls.
 */
@Component
public class ClosedPollValidator implements VoteValidator {

  @Override
  public VoteResult validate(Vote vote, Poll poll) {
    if (poll.isClosed()) {
      return VoteResult.rejected("This poll is closed. It's no longer accepting votes.");
    }
    return VoteResult.accepted();
  }
}
