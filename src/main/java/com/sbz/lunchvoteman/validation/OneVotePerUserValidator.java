package com.sbz.lunchvoteman.validation;

import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import org.springframework.stereotype.Component;

/**
 * Implements a validator that guarantees that a user only votes once in a Poll.
 */
@Component
class OneVotePerUserValidator implements VoteValidator {

  @Override
  public VoteResult validate(Vote vote, Poll poll) {
    VoteResult result = VoteResult.accepted();
    if (userAlreadyVotedInPoll(vote, poll)) {
      result = VoteResult.rejected(String.format("User %s already voted in this poll.", vote.getUser()));
    }
    return result;
  }

  private boolean userAlreadyVotedInPoll(Vote voteBeingValidated, Poll poll) {
    return poll.getVotes().stream().anyMatch(existingVote -> existingVote.getUser().equals(voteBeingValidated.getUser()));
  }
}
