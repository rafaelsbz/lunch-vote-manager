package com.sbz.lunchvoteman.validation;

import com.sbz.lunchvoteman.core.voting.VoteResult;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;

/**
 * Contract for components that validate votes in poll.
 */
interface VoteValidator {
  VoteResult validate(Vote vote, Poll poll);
}
