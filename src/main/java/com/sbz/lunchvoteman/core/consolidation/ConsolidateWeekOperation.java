package com.sbz.lunchvoteman.core.consolidation;

import com.sbz.lunchvoteman.persistence.repository.BlockedRestaurantRepository;
import com.sbz.lunchvoteman.rest.facade.ManagerRestFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Performs the steps necessary to consolidate a week in the system.
 */
@Component
public class ConsolidateWeekOperation {

  private static Logger LOGGER = LoggerFactory.getLogger(ManagerRestFacade.class);

  private final BlockedRestaurantRepository blockedRestaurantRepository;

  @Autowired
  public ConsolidateWeekOperation(BlockedRestaurantRepository blockedRestaurantRepository) {
    this.blockedRestaurantRepository = blockedRestaurantRepository;
  }

  /**
   * Performs week consolidation tasks. Currently, just clears the blocked restaurants collection.
   * <br><br>
   * 'Week' is also a misnomer, since this can be done in any length of time.   *
   */
  public void consolidateWeek() {
    LOGGER.info("Starting week consolidation");
    LOGGER.info("Clearing blocked restaurants");
    blockedRestaurantRepository.deleteAll();
    LOGGER.info("Week consolidation done.");
  }
}
