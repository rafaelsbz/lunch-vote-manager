package com.sbz.lunchvoteman.core.consolidation;

import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Responsible for calculating the current winner of a {@link Poll}.
 */
@Component
public class PollWinnerCalculator {

  /**
   * Returns the name of the restaurant with most votes in the Poll.
   */
  public String calculateWinner(Poll poll) {
    Map<String, Long> votesByRestaurant = poll.getVotes().stream().collect(Collectors.groupingBy(
        Vote::getRestaurant,
        Collectors.counting()));

    Optional<Map.Entry<String, Long>> winnerEntry = votesByRestaurant.entrySet()
        .stream()
        .max((entry, other) -> Long.compare(entry.getValue(), other.getValue()));

    if (winnerEntry.isPresent()) {
      return winnerEntry.get().getKey();
    }
    return "";
  }
}
