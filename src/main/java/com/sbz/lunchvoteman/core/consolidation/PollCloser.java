package com.sbz.lunchvoteman.core.consolidation;

import com.sbz.lunchvoteman.core.polls.PollRetriever;
import com.sbz.lunchvoteman.persistence.entities.BlockedRestaurant;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.repository.BlockedRestaurantRepository;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This component implements the steps involved closing a Poll.
 */
@Component
public class PollCloser {

  private final PollRetriever pollRetriever;
  private final PollRepository pollRepository;
  private final BlockedRestaurantRepository blockedRestaurantRepository;
  private final PollWinnerCalculator winnerCalculator;

  @Autowired
  public PollCloser(PollRetriever pollRetriever, PollRepository pollRepository,
      BlockedRestaurantRepository blockedRestaurantRepository,
      PollWinnerCalculator winnerCalculator) {
    this.pollRetriever = pollRetriever;
    this.pollRepository = pollRepository;
    this.blockedRestaurantRepository = blockedRestaurantRepository;
    this.winnerCalculator = winnerCalculator;
  }

  /**
   * Closes a poll and blocks the winner.
   */
  public void closePoll(String pollId) {
    //FIXME Implement better error handling for poll not found.
    Poll poll = pollRetriever.retrievePoll(pollId).get();
    String winner = winnerCalculator.calculateWinner(poll);

    poll.setClosed(true);
    pollRepository.save(poll);
    blockedRestaurantRepository.save(new BlockedRestaurant(winner));
  }
}
