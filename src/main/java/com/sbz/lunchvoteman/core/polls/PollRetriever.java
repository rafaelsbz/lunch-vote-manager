package com.sbz.lunchvoteman.core.polls;

import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Retrieves a poll from repository.
 */
@Component
public class PollRetriever {

  private final PollRepository repository;

  @Autowired
  public PollRetriever(PollRepository repository) {
    this.repository = repository;
  }

  /**
   * Retrieves a poll from the repository. If id is "current" (or any case variation of), retrieves
   * the most recent poll.
   */
  public Optional<Poll> retrievePoll(String id) {
    if (id.equalsIgnoreCase("current")) {
      return mostRecentPoll();
    } else {
      return repository.findById(id);
    }
  }

  private Optional<Poll> mostRecentPoll() {
    return repository.findAll(sortedByDate()).stream().findFirst();
  }

  private Sort sortedByDate() {
    return new Sort(Sort.Direction.DESC, "date");
  }
}
