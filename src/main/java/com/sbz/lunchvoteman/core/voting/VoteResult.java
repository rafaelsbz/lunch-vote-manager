package com.sbz.lunchvoteman.core.voting;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * Describes the result of a Vote. A registerVote can be accepted or rejected for many reasons.
 */
public class VoteResult {

  private final boolean accepted;
  private final List<String> reasons;

  public VoteResult(boolean accepted, List<String> reasons) {
    this.accepted = accepted;
    this.reasons = reasons;
  }

  public boolean isAccepted() {
    return accepted;
  }

  public List<String> getReasons() {
    return reasons;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof VoteResult)) {
      return false;
    }
    VoteResult other = (VoteResult) o;
    return accepted == other.accepted &&
        Objects.equal(reasons, other.reasons);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(accepted, reasons);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("accepted", accepted)
        .add("reasons", reasons)
        .toString();
  }

  // Sugar methods to improve readability.

  public static VoteResult accepted() {
    return new VoteResult(true, new ArrayList<>());
  }

  public static VoteResult rejected(List<String> messages) {
    return new VoteResult(false, messages);
  }

  public static VoteResult rejected(String... messages) {
    return VoteResult.rejected(Lists.newArrayList(messages));
  }
}
