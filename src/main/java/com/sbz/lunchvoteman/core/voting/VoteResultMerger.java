package com.sbz.lunchvoteman.core.voting;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BinaryOperator;

/**
 * Operator that merges two {@link VoteResult}s.
 */
@Component
public class VoteResultMerger implements BinaryOperator<VoteResult> {

  @Override
  public VoteResult apply(VoteResult result, VoteResult otherResult) {
    boolean bothAccepted = result.isAccepted() && otherResult.isAccepted();
    List<String> mergedReasons = new ArrayList<>();

    mergedReasons.addAll(result.getReasons());
    mergedReasons.addAll(otherResult.getReasons());

    return new VoteResult(bothAccepted, mergedReasons);
  }

}
