package com.sbz.lunchvoteman.core.voting;

import com.sbz.lunchvoteman.core.polls.PollRetriever;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import com.sbz.lunchvoteman.validation.VoteValidationSuite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Component that contains the logic of registering a Vote in a Poll.
 */
@Component
public class Voter {

  private static Logger LOGGER = LoggerFactory.getLogger(Voter.class);

  private final PollRetriever pollRetriever;
  private final PollRepository pollRepository;
  private final VoteValidationSuite voteValidationSuite;

  @Autowired
  public Voter(PollRetriever pollRetriever, PollRepository pollRepository, VoteValidationSuite voteValidationSuite) {
    this.pollRetriever = pollRetriever;
    this.pollRepository = pollRepository;
    this.voteValidationSuite = voteValidationSuite;
  }

  /**
   * Loads the poll, validates the vote and saves it if accepted.
   */
  public VoteResult registerVote(String pollId, Vote vote) {
    Optional<Poll> loadedPoll = pollRetriever.retrievePoll(pollId);
    VoteResult voteValidationResult = voteValidationSuite.validate(vote, loadedPoll);
    if (voteValidationResult.isAccepted()) {
      Poll poll = loadedPoll.get();
      poll.addVote(vote);
      pollRepository.save(poll);
      LOGGER.info("Accepted and saved vote {} in poll {}", vote, poll.getId());
    } else {
      LOGGER.info("Rejected vote {} for poll {} for the following reasons: {}", vote, pollId, voteValidationResult.getReasons());
    }
    return voteValidationResult;
  }
}
