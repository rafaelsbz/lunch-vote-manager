package com.sbz.lunchvoteman;

import com.sbz.lunchvoteman.core.consolidation.PollCloser;
import com.sbz.lunchvoteman.core.consolidation.PollWinnerCalculator;
import com.sbz.lunchvoteman.core.voting.Voter;
import com.sbz.lunchvoteman.persistence.entities.BlockedRestaurant;
import com.sbz.lunchvoteman.persistence.entities.Poll;
import com.sbz.lunchvoteman.persistence.entities.Restaurant;
import com.sbz.lunchvoteman.persistence.entities.Vote;
import com.sbz.lunchvoteman.persistence.repository.BlockedRestaurantRepository;
import com.sbz.lunchvoteman.persistence.repository.PollRepository;
import com.sbz.lunchvoteman.persistence.repository.RestaurantRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

/**
 * Application entry point. Runs the app.
 */
@Import(ApplicationConfiguration.class)
public class Main {
  public static void main(String[] args) {
    ConfigurableApplicationContext context = SpringApplication.run(Main.class);
  }
}
