package com.sbz.lunchvoteman;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Spring configuration for this app.
 */
@SpringBootApplication
public class ApplicationConfiguration {

}
