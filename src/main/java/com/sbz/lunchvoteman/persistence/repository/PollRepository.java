package com.sbz.lunchvoteman.persistence.repository;

import com.sbz.lunchvoteman.persistence.entities.Poll;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * Repository of conducted polls.
 */
public interface PollRepository extends MongoRepository<Poll, String> {

  Optional<Poll> findById(String id);

}
