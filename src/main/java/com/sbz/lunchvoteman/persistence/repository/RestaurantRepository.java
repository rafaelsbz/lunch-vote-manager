package com.sbz.lunchvoteman.persistence.repository;

import com.sbz.lunchvoteman.persistence.entities.Restaurant;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository that holds the known restaurants.
 */
public interface RestaurantRepository extends CrudRepository<Restaurant, String> {
}
