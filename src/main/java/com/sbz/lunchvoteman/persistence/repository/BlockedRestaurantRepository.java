package com.sbz.lunchvoteman.persistence.repository;

import com.sbz.lunchvoteman.persistence.entities.BlockedRestaurant;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository that keeps the restaurants currently isClosed.
 */
public interface BlockedRestaurantRepository extends CrudRepository<BlockedRestaurant, String> {
}
