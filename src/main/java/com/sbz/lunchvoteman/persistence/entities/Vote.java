package com.sbz.lunchvoteman.persistence.entities;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * A registerVote for a place to lunch.
 */
public class Vote {
  private final String user;
  private final String restaurant;

  public Vote(String user, String restaurant) {
    this.user = user;
    this.restaurant = restaurant;
  }

  public String getUser() {
    return user;
  }

  public String getRestaurant() {
    return restaurant;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Vote)) {
      return false;
    }
    Vote other = (Vote) o;
    return Objects.equal(this.user, other.user) &&
        Objects.equal(restaurant, other.restaurant);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(user, restaurant);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("user", user)
        .add("restaurant", restaurant)
        .toString();
  }
}
