package com.sbz.lunchvoteman.persistence.entities;

/**
 * Wrapper for the collection of isClosed restaurants. A Blocked Restaurant cannot be voted in a Poll.
 */
public class BlockedRestaurant extends Restaurant {

  public BlockedRestaurant(String name) {
    super(name);
  }
}
