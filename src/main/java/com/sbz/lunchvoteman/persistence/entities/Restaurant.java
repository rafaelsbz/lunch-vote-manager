package com.sbz.lunchvoteman.persistence.entities;

import com.google.common.base.Objects;
import org.springframework.data.annotation.Id;

/**
 * Represents a restaurant.
 */
public class Restaurant {

  @Id
  private final String name;

  public Restaurant() {
    this.name = "";
  }

  public Restaurant(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof  Restaurant)) {
      return false;
    }
    Restaurant that = (Restaurant) o;
    return Objects.equal(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(name);
  }

  @Override
  public String toString() {
    return String.format("Restaurant: %s", this.name);
  }
}
