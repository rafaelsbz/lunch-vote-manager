package com.sbz.lunchvoteman.persistence.entities;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.Sets;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * A poll that takes votes as to where people will have lunch.
 */
public class Poll {

  @Id
  private String id;

  private final Set<Vote> votes;

  private boolean closed;

  //TODO add time and implement "recent" logic using it.
  private final LocalDateTime date;


  public Poll() {
    this.votes = Sets.newHashSet();
    this.closed = false;
    this.date = LocalDateTime.now();
  }

  public Poll(String id, Set<Vote> votes, LocalDateTime date) {
    this.id = id;
    this.votes = votes;
    this.closed = false;
    this.date = date;
  }

  public Poll(String id) {
    this(id, Sets.newHashSet(), LocalDateTime.now());
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Set<Vote> getVotes() {
    return votes;
  }

  public void addVote(Vote vote) {
    votes.add(vote);
  }

  public void setClosed(boolean close) {
    this.closed = close;
  }

  public boolean isClosed() {
    return closed;
  }

  public LocalDateTime getDate() {
    return date;
  }


  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Poll)) {
      return false;
    }

    Poll other = (Poll) o;
    return id.equals(other.id) &&
        Objects.equal(votes, other.votes) &&
        Objects.equal(closed, other.closed) &&
        Objects.equal(date, other.date);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(id, votes, closed, date);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("id", id)
        .add("date", date)
        .add("isClosed", closed)
        .add("votes", votes)
        .toString();
  }
}
